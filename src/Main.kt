import kotlin.math.sqrt

fun main() {
    /* Point Actions */

    val point1 = Point(4.0F, 7.0F)
    val point2 = Point(-9.0F, 13.0F)
    println(point1.toString())
    println(point2.toString())
    println(point1.equals(point2))
    println(point1.movePoint())
    println(point2.movePoint())
    println(point1.calculateDistance(point2))

    /* Fraction Actions */

    val f1 = Fraction(5.0, 6.0)
    val f2 = Fraction(7.0, -8.0)
    println(f1.addition(f2))
    println(f2.subtraction(f2))
    println(f1.multiply(f2))
    println(f1.division(f2))
}




class Point(private var x:Float, private var y:Float){
    override fun toString(): String {
        return "$x, $y"
    }
    override fun equals(other: Any?): Boolean {
        if (other is Point) {
            if (x * other.y / y == x) {
                return true
            }
        }
        return false
    }

    fun movePoint(): String {
        return "${-x}, ${-y}"
    }

    fun calculateDistance(other: Any?): Any {
        if (other is Point) {
            val z = ((x - other.x) * (x - other.x)) + ((y - other.y) * (y - other.y))
            val distance = sqrt(z)
            return "$distance"
        }
        return false
    }
}

class Fraction(q: Double, r: Double) {
    private var numerator: Double = q
    private var denominator: Double = r

    override fun equals(other: Any?): Boolean {
        if (other is Fraction) {
            if (numerator * other.denominator / denominator == other.numerator) {
                return true
            }
        }
        return false
    }

    override fun toString(): String {
        return "$numerator / $denominator"
    }

    fun addition(other: Any?): Any {
        if (other is Fraction) {
            val axalimricxveli = denominator * other.denominator
            val axalimnishvenli =
                (axalimricxveli / denominator * numerator) - (axalimricxveli / other.denominator * other.numerator)
            val axaliwiladi = Fraction(axalimricxveli, axalimnishvenli)
            return axaliwiladi
        }
        return false
    }

     fun subtraction(other: Any?): Any? {
        if (other is Fraction) {
            val axalimricxveli = denominator * other.denominator
            val axalimnishvenli = (axalimricxveli / denominator * numerator) - (axalimricxveli / other.denominator * other.numerator)
            val axaliwiladi = Fraction(axalimricxveli, axalimnishvenli)
            return axaliwiladi
        }
        return false
    }

     fun multiply(other: Any?): Any? {
        if (other is Fraction) {
            val axalimricxveli = numerator * other.numerator
            val axalimnishvenli = denominator * other.denominator
            val axaliwiladi = Fraction(axalimricxveli, axalimnishvenli)
            return axaliwiladi
        }
        return false
    }

     fun division(other: Any?): Any {
        if(other is Fraction) {
            val axalimricxveli = numerator * other.denominator
            val axalimnishvneli = denominator * other.numerator
            val axaliwiladi = Fraction(axalimricxveli, axalimnishvneli)
            return axaliwiladi
        }
        return false
    }
}

